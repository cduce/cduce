#load "all.cma";;
open Types;;

let recurs f =
  let t = make () in
  define t (f t);
  internalize t

let cons d =
  let t = make () in
  define t d;
  internalize t
  

let list t = recurs (fun s -> cup (times t s) (atom 0));;
let square i j k l = times (cons (interval i j)) (cons (interval k l))
let int_list i j = descr (list (cons (interval i j)))

let t1 = recurs (fun t -> times t t);;
let t2 = recurs (fun t -> cup (times t t) (interval 1 2));;
let t3 = recurs (fun t -> record 1 false t2);;
let t4 = recurs (fun t -> arrow t t);;
let t5 = recurs (fun t -> times t3 t4);;
let t6 = cons 
	   (
	     diff 
	      (square 1 2 1 2)
	      (cup (square 1 2 1 1) (square 1 1 1 2))
	   );;

let t7 = cons (diff (int_list 1 2) (int_list 1 1));;

Sample.get (descr t7);;

(*

open Syntax

let v = new_var ();;
let t = cons (Or (cons (Variable v), cons (Capture "x")));;
global v t;;


*)
let t8 = cons (diff (diff 
		 (string (`Star (`Char (65,65)))) 
		 (string `Eps)
		    )
	       (string (`Char (65,65)))
	      );;
is_empty (descr t8);;
Sample.get (descr t8);;


let sample typ = 
  let s = Syntax.parse typ in
  let t = Syntax.make_type s in
  Types.Sample.get (Types.descr t);;

(* To fix: 
let `Type t = Syntax.make (Syntax.parse "/ 'a'+ 'b'? 'c'+ /");;
let `Type t = Syntax.make (Syntax.parse "/ 'a'+ 'b' /");;
let `Type t = Syntax.make (Syntax.parse "/ 'a'+ 'b'? 'c'+ | 'd'+ /");;
Sample.get t;;
*)

sample "'a where 'a = (`X,'a) | (`X,`Y)";;

