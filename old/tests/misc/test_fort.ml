open Types;;
open Fort;;

open Parser.From_string

let (=?) a b = expect_equal a b;;
let (??) a = expect_true a;;

let parse_typ s = Types.descr (Typer.typ (pat s));;

expect_pass "Parsing types"
  (fun () ->
     
     let t1 = parse_typ "[1 -- 10 *]" in
     let t2 = parse_typ "[20 -- 30 +]" in
     
     ()
  );;


let recurs f =
  let t = make () in
  define t (f t);
  internalize t

let recurs' f = 
  let t = make () in
  define t (f t);
  t

let cons d =
  let t = make () in
  define t d;
  internalize t

let cons' d =
  let t = make () in
  define t d;
  t

let list = Sequence.star
let int i = Intervals.mk (string_of_int i)
let interv i j = Types.interval (Intervals.bounded (int i) (int j))
let square i j k l = times (cons (interv i j)) (cons (interv k l))

(*

let int_list i j = descr (list (cons (interval i j)))
let int i = interval i i

let (---) = diff;;
let (=?) a b = expect_equal a b;;
let (??) a = expect_true a;;

exception NoException
let exn a x e = 
  try ignore (a x); raise NoException
  with 
    | NoException -> fail "No exception"
    | e' -> 
	if e' <> e then fail 
	  (Printf.sprintf "Wrong exception: '%s' instead of '%s'" 
	     (Printexc.to_string e')
	     (Printexc.to_string e)
	  )


expect_pass "Unique representation"
(fun () ->
   let t1 = recurs (fun t -> times t t) in
   let t2 = recurs (fun t -> times t1 t) in
   let t3 = recurs (fun t -> arrow t t) in
   let t4 = recurs (fun t -> times t (cons' (times t t))) in
   let t5 = recurs (fun t -> cup (times t t) (times t t)) in
   id t1 =? id t2;
   id t1 =? id t4;
   id t1 =? id t5;
   ?? (id t3 <> id t2);
);;

expect_pass "Interval"
(fun () ->
   let t = int_list 1 2 --- int_list 1 1 in
   Sample.get t =? Sample.Pair (Sample.Int 2, Sample.Atom 0);
   Sample.get (interval 10 20) =? Sample.Int 10;
);;

expect_pass "Record"
(fun () ->
   let r l o i j = record l o (cons (interval i j)) in
   Sample.get (r 1 false 100 200) =? Sample.Record [(1, Sample.Int 100 )];
   Sample.get (r 1 true 100 200) =? Sample.Record [];
   Sample.get (r 1 false 100 200 --- r 1 false 100 150) 
       =? Sample.Record [(1, Sample.Int 151 )];
   exn Sample.get (r 1 false 100 200 --- r 1 false 100 250) Not_found;
);;


expect_pass "Regular string expressions" 
(fun () ->
   let t = string (Strings.Star (Strings.Char 'a')) ---
	   string Strings.Eps ---
	   string (Strings.Char 'a')
   in
   is_empty t =? false;
   Sample.get t =? Sample.String "aa";
);;

*)
