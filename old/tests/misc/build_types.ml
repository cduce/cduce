
  (* some experiments with hand construction of CDuce types *)

let xml =
  Types.xml'
    (Types.atom (Atoms.atom (Atoms.mk_ascii "xml")))
    Types.empty_closed_record
    Types.Int.any
;;
let choice =
  Types.choice_of_list [ Builtin_defs.bool; Builtin_defs.string; Builtin_defs.int ]
;;
let seq =
  Sequence.seq_of_list [ Builtin_defs.bool; Builtin_defs.string; Builtin_defs.int ]
;;
let reco =  (* closed record with two fields *)
  Types.rec_of_list ~opened:false ["foo", Builtin_defs.int; "bar", Builtin_defs.bool]
;;
let opt_reco =  (* closed record with two required and one optional fields *)
  Types.rec_of_list' ~opened:false [
    false, "foo", Builtin_defs.int;
    false, "bar", Builtin_defs.bool;
    true, "baz", Builtin_defs.string;
  ]
;;
let concat =  (* TODO ... ask Alain: how to concatenate two star types? *)
  let int_star = (Sequence.star Builtin_defs.bool) in
  let bool_star = (Sequence.star Builtin_defs.int) in
(*   Sequence.concat int_star bool_star *)
  Sequence.flatten (Sequence.seq_of_list [ int_star; bool_star ])
;;
let rex =
  let elem = Ast.Elem (Location.mknoloc (Ast.Internal xml)) in
  let nil = Location.mknoloc (Ast.Internal Sequence.nil_type) in
  let rex =
    (Ast.Seq ((Ast.Seq (Ast.Star elem, elem)), (Ast.Seq (elem, Ast.Star elem))))
  in
  let ast_rex = Location.mknoloc (Ast.Regexp (rex, nil)) in
  print_endline (Ast.string_of_regexp rex);
  Types.descr (Typer.typ ast_rex)
(*   Typer.typ' (Typer.real_compile (Typer.derecurs Typer.TypeEnv.empty ast_rex)) *)
;;

let string = Sequence.star Types.Char.any ;;
Types.Print.print Format.std_formatter rex;
Format.fprintf Format.std_formatter "\n"

