(* An approximation of HTML *)

type Flow = Char | Block | Inline | Misc;;
type Block = P | Heading | Div | Lists | Table | Blocktext;;
type Lists = Ul;;
type Blocktext = Pre | Hr | Blockquote | Address;;
type Inline = Char | A | Special | Fontstyle | Phrase;;
type Fontstyle = Tt | I | B | Big | Small;;
type Phrase = Em | Strong | Code;;
type Special = Br;;
type Misc = Empty;;

type Html = <html>[ Head Body ];;
type Head = <head>[ Title ];;
type Title = <title>[ PCDATA ];;
type Body = <body bgcolor=String>[ Block* ];;

type Div = <div>[ Flow* ];;
type P = <p>[ Inline* ];;
type Heading = <(`h1 | `h2)>[ Inline* ];;

type Ul = <ul>[Li+];;
type Li = <li>[ Flow* ];;

type Address = <address>[ Inline* ];;
type Hr = <hr>[];;
type Pre = <pre>[ (PCDATA | A | Fontstyle | Phrase | Br)* ];;
type Blockquote = <blockquote>[ Block* ];;

type A = <a ({ name = String } | { href = String })>[ (Inline \ A)* ];;
type Br = <br>[];;
type Em = <em>[ Inline* ];;
type Code = <code>[ Inline* ];;
type Strong = <strong>[ Inline* ];;
type Tt = <tt>[ Inline* ];;
type I = <i>[ Inline* ];;
type B = <b>[ Inline* ];;
type Big = <big>[ Inline* ];;
type Small = <small>[ Inline* ];;

type Table = <table border=?String; bgcolor=String; width=String>
     [ <tr>[ <td>[ Flow* ]+ ]+ ];;



(* Input document *)

type Page = <page filename=Latin1>[ <title>String; Content ];;
type Content = [ (Box | Section)* ];;

type Box = <box>Text;;
type Section = <section no=?Int>[ <title>String ; Text ];;

type Text = [ (Char | <duce>String | <ul>[<li>Text +] 
	      | <a href=String>String | <br>[])* ];;


let fun box(c : [Flow*]) : [Block*] = 
 [ <table bgcolor="white"; width="100%"; border="1">[ <tr>[ <td>c ] ] 
    <p>[] ];;

let fun format (Box | Section | Content -> [Block*]; Text -> [Flow*])
 | <box>s -> box (format s)
 | <section no=i>[ <title>t ; s ] -> 
	    box [ <h2>[!(string_of i) '. ' !t] <a name=(string_of i)>[];
		  format s ]
 | txt & Text -> (map txt with 
                   | <duce>c -> <b>[<tt>c]
		   | <ul>l -> <ul>(map l with <li>c -> <li>(format c) )
		   | c -> c)
 | c & Content -> (transform c with x -> format x)
 | _ -> `nil;;

let fun summary (Content -> [Block*])
 c -> 
    let s = transform c with <section no=i>[<title>t; _] -> 
       [<li>[<a href=("#" @ string_of i)>t]] in
    match s with [] -> [] | lis -> box [<ul>lis];;


let (fname, title, content) =
  match load_xml "tests/misc/memento.xml" with
  | <page filename=f>[ <title>t ; c ] & Page ->
    let fun aux ((Int,Content) -> Content)
      | (_,[]) -> []
      | (i,(<section>s,rem)) -> [ <section no=i>s !(aux (i+1,rem)) ]
      | (i,(x,rem)) -> [ x !(aux (i,rem)) ]
	  in
	  (f,t,aux (1,c))
  | _ -> raise "Invalid input document!";;

let out : Html = 
 <html>[
   <head>[ <title>title ]
   <body bgcolor="#BBDDFF">
     [ !(box [<h1>title]) !(summary content) !(format content) ]
 ];;

dump_to_file fname (print_xml out);;
