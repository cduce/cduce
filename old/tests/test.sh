#!/bin/sh

# Variables

# Path from caller to script. TODO: Some tests won't work if the script is
# called from another place than the root directory of the project.
ROOT=`dirname $0`

# false: normal test suite (default), true: extended test suite. Do not modify
# this variable directly. To run the extended test suite, use the "-e" option.
# The extended test suite contains the normal test suite plus broken tests to
# fix and tests on features that are not supported by CDuce (yet).
EXTENDED="false"

# Error code: 0 success; 1 failure.
RET=0

# Parse options

while test $# -ne 0; do
    if test $1 = "-e" || test $1 = "--extended"; then EXTENDED="true"; fi;
    shift;
done

# Test schema

SCHEMADIR=$ROOT/schema
SCHEMATESTS="$SCHEMADIR/library1.xsd $SCHEMADIR/library2.xsd\
 $SCHEMADIR/library3.xsd $SCHEMADIR/mails.xsd $SCHEMADIR/po.xsd\
 $SCHEMADIR/po2.xsd"
SCHEMASCRIPT=$SCHEMADIR/test.pl
SCHEMACDFILE=$SCHEMADIR/test.cd
SCHEMALOGFILE=$SCHEMADIR/test.log

echo -n "Tests on schema: "
if test $EXTENDED = "false"; then
    $SCHEMASCRIPT $SCHEMACDFILE $SCHEMALOGFILE $SCHEMATESTS
else
    $SCHEMASCRIPT $SCHEMACDFILE $SCHEMALOGFILE $SCHEMADIR/*.xsd
fi
test $? -ne 0 && RET=1
echo " passed."

# Test regtest

REGDIR=$ROOT/schema/regtest
REGSCRIPT=$SCHEMADIR/test.pl
REGCDFILE=$REGDIR/test.cd
REGLOGFILE=$REGDIR/test.log

echo -n "Tests on regtest: "
$REGSCRIPT $REGCDFILE $REGLOGFILE $REGDIR/*.xsd
test $? -ne 0 && RET=1
echo " passed."

# Test xsltmark

SUCCESS=0
TOTAL=0
XSLTLOG=$ROOT/xsltmark/log
GOODTESTS="$ROOT/xsltmark/identity.cd"
EXTENDEDTESTS="$ROOT/xsltmark/xslbench2.cd $ROOT/xsltmark/xslbench4.cd"

echo -n "Tests on xsltmark: "

echo > $XSLTLOG

if test $EXTENDED = "true"; then GOODTESTS="$GOODTESTS $EXTENDEDTESTS"; fi

for i in db100.xml db10.xml; do
    TOTAL=$(($TOTAL+1))
    echo "==============dbonerow.cd --arg $i===============" >> $XSLTLOG;
    $ROOT/../cduce $ROOT/xsltmark/dbonerow.cd --arg $ROOT/xsltmark/testcases/$i > /dev/null 2>> $XSLTLOG;
    if test $? -eq 0; then SUCCESS=$(($SUCCESS+1)); fi
done

for i in $GOODTESTS; do
    TOTAL=$(($TOTAL+1))
    echo "==============`echo $i | rev | cut -d "/" -f -1 | rev`===============" >> $XSLTLOG;
    $ROOT/../cduce $i > /dev/null 2>> $XSLTLOG;
    if test $? -eq 0; then SUCCESS=$(($SUCCESS+1)); fi
done

echo "$SUCCESS / $TOTAL passed."
test $SUCCESS -ne $TOTAL && RET=1

# Test cql

SUCCESS=0
TOTAL=0
CQLLOG=$ROOT/cql/log
GOODTESTS="$ROOT/cql/TREE/* $ROOT/cql/XMP/*"
EXTENDEDTESTS="$ROOT/cql/R/* $ROOT/cql/SEQ/*"

echo -n "Tests on cql: "

echo > $CQLLOG

if test $EXTENDED = "true"; then GOODTESTS="$GOODTESTS $EXTENDEDTESTS"; fi

for i in $GOODTESTS; do
    TOTAL=$(($TOTAL+1))
    echo "==============`echo $i | rev | cut -d "/" -f -1 | rev`===============" >> $CQLLOG;
    $ROOT/../cduce $i > /dev/null 2>> $CQLLOG;
    if test $? -eq 0; then SUCCESS=$(($SUCCESS+1)); fi
done

echo "$SUCCESS / $TOTAL passed."
test $SUCCESS -ne $TOTAL && RET=1

# Test misc

echo -n "Tests on misc: "

SUCCESS=0
TOTAL=0
MISCLOG=$ROOT/misc/log
GOODTESTS="$ROOT/misc/addrbook.cd $ROOT/misc/biblio.cd\
 $ROOT/misc/fixml4.3v20020920.cd $ROOT/misc/funs.cd $ROOT/misc/IFX130.cd\
 $ROOT/misc/integers.cd $ROOT/misc/lazy.cd $ROOT/misc/mc.cd\
 $ROOT/misc/memento.cd $ROOT/misc/notes.cd $ROOT/misc/ns.cd\
 $ROOT/misc/overloading.cd $ROOT/misc/overloading2.cd $ROOT/misc/patterns.cd\
 $ROOT/misc/security.cd $ROOT/misc/str.cd $ROOT/misc/stress_opt_arg.cd\
 $ROOT/misc/stress_opt_seq.cd $ROOT/misc/vouill.cd $ROOT/misc/web.cd\
 $ROOT/misc/wp.cd $ROOT/misc/xtrans.cd $ROOT/misc/ref.cd $ROOT/misc/xhtml.cd"
BADTESTS="$ROOT/misc/bugs_prod.cd $ROOT/misc/eval_concat.cd"
EXTENDEDTESTS="$ROOT/misc/html2latex.cd"

echo > $MISCLOG

if test $EXTENDED = "true"; then GOODTESTS="$GOODTESTS $EXTENDEDTESTS"; fi

for i in $GOODTESTS; do
    TOTAL=$(($TOTAL+1))
    echo "==============`echo $i | rev | cut -d "/" -f -1 | rev`===============" >> $MISCLOG;
    $ROOT/../cduce $i > /dev/null 2>> $MISCLOG;
    if test $? -eq 0; then SUCCESS=$(($SUCCESS+1)); fi
done

for i in $BADTESTS; do
    TOTAL=$(($TOTAL+1))
    echo "==============`echo $i | rev | cut -d "/" -f -1 | rev`===============" >> $MISCLOG;
    $ROOT/../cduce $i > /dev/null 2>> $MISCLOG;
    if test $? -ne 0; then SUCCESS=$(($SUCCESS+1)); fi
done

echo "$SUCCESS / $TOTAL passed."
test $SUCCESS -ne $TOTAL && RET=1

# Test ocamlinterface

OCAMLDIR=$ROOT/ocaml
MAKECONF=$ROOT/../Makefile.conf

WITHOCAML=false
test -f $MAKECONF && WITHOCAML=`cat $MAKECONF | grep ML_INTERFACE | cut -d '=' -f 2`

if test $WITHOCAML = "true"; then

    echo "Tests on ocamlinterface: "
    ocamlc -I $OCAMLDIR/misc -c $OCAMLDIR/misc/misc.mli
    $ROOT/../cduce -I $OCAMLDIR/misc --compile $OCAMLDIR/misc/consts.cd
    $ROOT/../cduce -I $OCAMLDIR/misc --compile $OCAMLDIR/misc/misc.cd
    $ROOT/../cduce -I $OCAMLDIR/misc --mlstub $OCAMLDIR/misc/consts.cdo > $OCAMLDIR/misc/consts.ml
    $ROOT/../cduce -I $OCAMLDIR/misc --mlstub $OCAMLDIR/misc/misc.cdo > $OCAMLDIR/misc/misc.ml
    ocamlfind ocamlc -I $OCAMLDIR/misc -g -package ocaml-compiler-libs.common,cduce,oUnit -linkpkg -o $OCAMLDIR/misc/misc $OCAMLDIR/misc/consts.ml $OCAMLDIR/misc/misc.ml $OCAMLDIR/misc/misctest.ml
    $OCAMLDIR/misc/misc 2> /dev/null
    test $? -ne 0 && RET=1

    ocamlc -I $OCAMLDIR/eval -c $OCAMLDIR/eval/eval.mli
    $ROOT/../cduce -I $OCAMLDIR/eval --compile $OCAMLDIR/eval/eval.cd -I $OCAML_TOPLEVEL_PATH/../cduce
    $ROOT/../cduce -I $OCAMLDIR/eval --mlstub $OCAMLDIR/eval/eval.cdo > $OCAMLDIR/eval/eval.ml
    ocamlfind ocamlc -I $OCAMLDIR/eval -package ocaml-compiler-libs.common,cduce,oUnit -linkpkg -o $OCAMLDIR/eval/eval $OCAMLDIR/eval/eval.ml $OCAMLDIR/eval/evaltest.ml
    $OCAMLDIR/eval/eval
    test $? -ne 0 && RET=1

    ocamlc -I $OCAMLDIR/cdnum -c $OCAMLDIR/cdnum/cdnum.mli
    $ROOT/../cduce -I $OCAMLDIR/cdnum --compile $OCAMLDIR/cdnum/cdnum.cd -I `ocamlfind query num`
    $ROOT/../cduce -I $OCAMLDIR/cdnum --mlstub $OCAMLDIR/cdnum/cdnum.cdo > $OCAMLDIR/cdnum/cdnum.ml
    ocamlfind ocamlc -I $OCAMLDIR/cdnum -package ocaml-compiler-libs.common,cduce,num,oUnit -linkpkg -o $OCAMLDIR/cdnum/cdnum $OCAMLDIR/cdnum/cdnum.ml $OCAMLDIR/cdnum/cdnumtest.ml
    $OCAMLDIR/cdnum/cdnum
    test $? -ne 0 && RET=1

    if test $EXTENDED = "true"; then
	ocamlc -I $OCAMLDIR/cdsdl -c -I `ocamlfind query sdl` $OCAMLDIR/cdsdl/cdsdl.mli
	$ROOT/../cduce -I $OCAMLDIR/cdsdl --compile -I `ocamlfind query sdl` $OCAMLDIR/cdsdl/cdsdl.cd
	$ROOT/../cduce -I $OCAMLDIR/cdsdl --mlstub -I `ocamlfind query sdl` $OCAMLDIR/cdsdl/cdsdl.cdo > $OCAMLDIR/cdsdl/cdsdl.ml
	ocamlfind ocamlc -I $OCAMLDIR/cdsdl -package ocaml-compiler-libs.common,cduce,sdl,oUnit -linkpkg -o $OCAMLDIR/cdsdl/cdsdl $OCAMLDIR/cdsdl/cdsdl.ml $OCAMLDIR/cdsdl/cdsdltest.ml
	$OCAMLDIR/cdsdl/cdsdl
	test $? -ne 0 && RET=1

	ocamlc -I $OCAMLDIR/latypes -c $OCAMLDIR/latypes/latypes.mli
	$ROOT/../cduce -I $OCAMLDIR/latypes --compile $OCAMLDIR/latypes/latypes.cd
	$ROOT/../cduce -I $OCAMLDIR/latypes --mlstub $OCAMLDIR/latypes/latypes.cdo > $OCAMLDIR/latypes/latypes2.ml
	ocamlfind ocamlc -I $OCAMLDIR/latypes -package ocaml-compiler-libs.common,cduce,num,oUnit -linkpkg -o $OCAMLDIR/latypes/latypes $OCAMLDIR/latypes/latypes.ml $OCAMLDIR/latypes/latypes2.ml $OCAMLDIR/latypes/latypestest.ml
	$OCAMLDIR/latypes/latypes
	test $? -ne 0 && RET=1
    fi

else
    echo -n "Warning: The interface with OCaml isn't tested as it has not been"
    echo -n " built. Check the \`mliface' option in the configure for more"
    echo " information."
fi # endif $WITHOCAML = "true"

exit $RET
