val id : Cduce_lib.Core.Value.t -> Cduce_lib.Core.Value.t
val add : Big_int.big_int -> Big_int.big_int -> Big_int.big_int
val sub : Big_int.big_int -> Big_int.big_int -> Big_int.big_int
type t = A of (Big_int.big_int * Big_int.big_int) | B
val t_to_int : t -> Big_int.big_int
