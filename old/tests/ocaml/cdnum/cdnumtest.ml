open OUnit2
open Big_int

let tests = "Cdnum" >:::
  [
    "fact" >:: ( fun test_ctxt ->
      assert_equal ~msg:"Test Cdnum.fact.1 failed"
	2 (int_of_big_int (Cdnum.fact (big_int_of_int 2)));
      assert_equal ~msg:"Test Cdnum.fact.2 failed"
	1 (int_of_big_int (Cdnum.fact (big_int_of_int 0)));
      assert_equal ~msg:"Test Cdnum.fact.3 failed"
	120 (int_of_big_int (Cdnum.fact (big_int_of_int 5)));
    );
  ]

let _ = run_test_tt_main tests
