open OUnit2

let tests = "Latypes" >:::
  [
    "f" >:: ( fun test_ctxt ->
      assert_equal ~msg:"Test Latypes.f.1 failed"
	(Some 2) Latypes.b (Latypes.a 2);
    );
  ]

let _ = run_test_tt_main tests
