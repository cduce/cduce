                 Installation Notes for Windows XP/MinGW
                 =======================================

It is possible to compile CDuce under cygwin with the MinGW toolchain.
Currently, the resulting executable still depends on the cygwin1.dll.
The suspect is pcre, but the source of the "pollution" has not been
determined.

The dependencies can be checked with the "cygcheck" command under
cygwin.  Note that cygcheck searches in the PATH, i.e., if a binary is
not in the path, one must give the full path.  To check a binary in
the mingw directory (e.g., C:\ocamlmgw\bin\cduce), one must give the
Mingw path (C:/ocamlmgw/bin/cduce) to cygcheck, not the cygwin path
(/cygdrive/c/ocamlmgw/bin).


When compiling under the MinGW toolchain, the option -mno-cygwin must
always be passed to the C compiler and linker.  It is also necessary
to ensure that one is not linking against any cygwin libraries at any
point.  The MinGW libraries are located in /usr/lib/...


Steps:

----------------------------------------------------------------------
1) In Cygwin, add the packages for MinGW support to the ones already
   mentioned in INSTALL.WIN32.

   gcc-mingw-core
   mingw-runtime
   w32-api
   <maybe more, which may be the cause of our problem?>
   
   Remove the prce packages from Cygwin, to make sure that the pcre
   used is the one installed under Mingw.


----------------------------------------------------------------------
2) Install tcl/tk in Windows

   Install Tcl/Tk in a windows top directory, eg, C:\Tcl

   binaries available as described in ocaml notes (README.win32).


----------------------------------------------------------------------
3) OCAML

Build ocaml 3.10 for the mingw toolchain as specified in README.win32,
but with the following addition from the ocaml mailing list:

    From: Arnold Guillaumot <arnold.guillaumot <at> snef.ro>
    Subject: Re: Compiling with Cygwin/MinGW
    Newsgroups: gmane.comp.lang.ocaml.lib.gtk
    Date: 2007-09-05 18:55:39 GMT

    Hi,

    I succeeded in the past to build ocaml and lablgtk with MinGW/MSYS
    (without Cygwin), together with ocamlmklib.
    In the ocaml tree, change the following files:

    1) ocamlmklib

    The 'trick' is to copy the Makefile part of ocamlmklib in the Makefile.nt

    edit tools/Makefile.nt and change the Line 26
         all: ocamldep ocamlprof ocamlcp.exe ocamlmktop.exe primreq
         in
         all: ocamldep ocamlprof ocamlcp.exe ocamlmktop.exe primreq
    ocamlmklib.exe

    append the following lines after the ocamlmktop part:

    # To help building mixed-mode libraries (Caml + C)

    ocamlmklib.exe: myocamlbuild_config.cmo ocamlmklib.cmo
        $(CAMLC) $(LINKFLAGS) -o ocamlmklib.exe myocamlbuild_config.cmo
    ocamlmklib.cmo

    ocamlmklib.cmo: myocamlbuild_config.cmi
    myocamlbuild_config.ml: ../config/Makefile
        ../build/mkmyocamlbuild_config.sh
        cp ../myocamlbuild_config.ml .

    install::
        cp ocamlmklib.exe $(BINDIR)/ocamlmklib.exe

    clean::
        rm -f ocamlmklib.exe

    ocamlmklib.ml: ocamlmklib.mlp ../config/Makefile
        echo '(* THIS FILE IS GENERATED FROM ocamlmklib.mlp *)' >ocamlmklib.ml
        sed -e "s|%%BINDIR%%|$(BINDIR)|" \
                -e
    "s|%%SUPPORTS_SHARED_LIBRARIES%%|$(SUPPORTS_SHARED_LIBRARIES)|" \
                -e "s|%%MKSHAREDLIB%%|$(MKSHAREDLIB)|" \
                -e "s|%%BYTECCRPATH%%|$(BYTECCRPATH)|" \
                -e "s|%%NATIVECCRPATH%%|$(NATIVECCRPATH)|" \
                -e "s|%%MKSHAREDLIBRPATH%%|$(MKSHAREDLIBRPATH)|" \
                -e "s|%%RANLIB%%|$(RANLIB)|" \
              ocamlmklib.mlp >> ocamlmklib.ml

    beforedepend:: ocamlmklib.ml

    clean::
        rm -f ocamlmklib.ml

It seems that this is not in the ocaml distribution because it doesn't
work under MSVS, only under MinGW, and since there is only one
Makefile.nt...


adjust PATH:

PATH=$PATH:/cygdrive/c/ocamlmgw/bin


----------------------------------------------------------------------
4) findlib

  Patch configure replacing 
    stdlib = `get_stdlib`
  with 
    stdlib = /cygdrive/c/ocamlmgw/lib

  ./configure -bindir /cygdrive/c/ocamlmgw/bin

  Patch Makefile.config replacing
    OCAML_CORE_MAN = /usr/local/man
  with
    OCAML_CORE_MAN = /cygdrive/c/ocamlmgw/man
  (since ocaml for mingw does not install the man directories, the
  mandir option does not work).

  make all
  make opt
  make install


----------------------------------------------------------------------
5) ulex

This gets stuck with the error "can't found ocamlbuildlib.cma in
ocamlbuild -where", which is not surprising, since ocamlbuild -where
returns /ocamlbuildlib/lib, which doesn't exist.  See ocaml bug 4379.
This is supposedly fixed in ocaml 3.10.1, but it still doesn't seem to
work.

Until this bug is fixed, patch Makefile to use 

  ocamlbuild -install-lib-dir "C:\ocamlmgw\lib\ocamlbuild"

(or whereever your mingw installation is).  This workaround is
suitable for cygwin, but it may not work in Mingw/MSYS.

Remember to also add -classic-display to ocamlbuild, since we are
building with ocaml 3.10.

  make all
  make all.opt
  make install


----------------------------------------------------------------------
6) PCRE:

Since the pcre that comes with cygwin cannot be used in mingw, install
pcre before pcre-ocaml.  This is simpler if pcre is not installed in
cygwin.

Go back to cygwin's setup.exe and install gcc-g++ (C++ compiler).

Download pcre-7.2 from www.pcre.org or
ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre

To configure pcre-7.2:

If pcre is installed in cygwin, patch ltmain.sh so that need_relink is
always "no", that may avoid the cygwin dependency.

./configure --prefix="C:/ocamlmgw" --exec-prefix="C:/ocamlmgw" --includedir=/usr/include/mingw --disable-cpp --enable-utf8 CFLAGS="-mno-cygwin -I /usr/include/w32api -I /usr/include/mingw"

Patch the Makefile to replace cygwin with mingw everywhere it occurs
(except in -mno-cygwin).

Patch the libtool file similarly.

make

make install

This installs the libs in the ocaml mingw directory
(e.g. C:\ocamlmgw\lib, but the .h files in /usr/include/mingw.  This
should be suitable for continued compilation using the mingw
toolchain, but not for a pure mingw compilation.

Check with cygcheck whether, eg, pcregrep.exe, depends on the
cygwin1.dll and cygpcre-0.dll.
----------------------------------------------------------------------
7) pcre-ocaml:

Edit Makefile.conf to contain

export STATIC = yes
export INCDIRS := /usr/include/mingw
export LIBDIRS := C:/ocamlmgw/lib
MINGW=1
CC=gcc
export GCC
export MINGW

(If cc is not set, it defaults to cc, which causes 'make' to fail
spectacularly during

ocamlc -c -cc "cc" -ccopt "-fPIC -O2 -DPIC -mno-cygwin -I /usr/include/mingw -o pcre_stubs.o" pcre_stubs.c

sometimes even to the point of crashing cygwin.)

make
make install

The file pcre_make.win32/Makefile.mingw does not seem to work, either
it is for Mingw/MSYS or it has not been updated.

The file README.win32 concerns MS Visual Studio, i.e., it is
irrelevant.

To check that pcre actually works in ocaml:

  make a file foo.ml that uses Pcre, eg

  open Prce;;
  qreplace;;

  compile it with 

  ocamlc -I "C:/ocamlmgw/lib/site-lib/pcre" foo.ml

----------------------------------------------------------------------
8) pxp

   ./configure -without-wlex -without-wlex-compat -lexlist utf8,iso88591 -without-pp

   make all
   make opt
   make install


----------------------------------------------------------------------
9) CDuce

./configure --prefix="C:/ocamlmgw" --mliface="C:/cygwin/usr/src/ocaml-3.10.0/" 

patch Makefile.conf to set FORPACK=true
(For some reason that fails in configure.ml)

Patch Makefile.distrib to set

LINK = $(CAMLOPT) -custom -linkpkg

in line 71 to statically link the library.


make all
make install
