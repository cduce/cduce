open Types

type rhs =
  [ `Type of descr
  | `Variable of Var.t
  | `Cup of v list
  | `Cap of v list
  | `Times of v * v
  | `Xml of v * v
  | `Arrow of v * v
  | `Record of (Ident.label * bool * v) list * bool
  | `Diff of v * v ]

and v = { id : int; mutable def : rhs; mutable node : Node.t option }

module MemHash = Hashtbl.Make (struct
  type t = Types.t

  let equal = ( == )

  let hash = Types.hash
end)

let op_l init op = function
  | []      -> init
  | e :: ll -> List.fold_left (fun acc e -> op acc e) e ll

let rec make_descr seen v =
  if List.memq v seen then empty
  else
    let seen = v :: seen in
    match v.def with
    | `Variable v          -> Types.var v
    | `Type d              -> d
    | `Cup vl              -> op_l empty cup (List.map (make_descr seen) vl)
    | `Cap vl              -> op_l any cap (List.map (make_descr seen) vl)
    | `Times (v1, v2)      -> times (make_node v1) (make_node v2)
    | `Xml (v1, v2)        -> xml (make_node v1) (make_node v2)
    | `Arrow (v1, v2)      -> arrow (make_node v1) (make_node v2)
    | `Record (fields, op) ->
        let tfields =
          List.map
            (fun (l, abs, v) ->
              let t = make_descr seen v in
              let n = cons (if abs then Record.or_absent t else t) in
              (l, n))
            fields
        in
        record_fields
          (op, Ident.LabelMap.from_list (fun _ _ -> assert false) tfields)
    | `Diff (v1, v2)       -> (
        match (v1.def, v2.def) with
        | _, `Cup [] -> make_descr seen v1
        | _, `Cap [] -> Types.empty
        | _          -> diff (make_descr seen v1) (make_descr seen v2))

and make_node v =
  match v.node with
  | Some n -> n
  | None   ->
      let n = make () in
      v.node <- Some n;
      let d = make_descr [] v in
      define n d;
      n

let dump ppf v =
  let seen = Hashtbl.create 17 in
  let open Format in
  let pp_sep ppf () = fprintf ppf ",@ " in
  let rec loop ppf v =
    if Hashtbl.mem seen v.id then fprintf ppf "X_%d" v.id
    else begin
      Hashtbl.add seen v.id ();
      fprintf ppf "X_%d=" v.id;
      loop_def v
    end
  and loop_def v =
    match v.def with
    | `Type d              -> fprintf ppf "@[`Type(%a)@]" Print.print d
    | `Variable v          -> fprintf ppf "@[`Variable(%a)@]" Var.print v
    | `Cup l               -> fprintf ppf "@[`Cup(%a)@]" pp_lst l
    | `Cap l               -> fprintf ppf "@[`Cap(%a)@]" pp_lst l
    | `Times (v1, v2)      -> pp_binop ppf "`Times" v1 v2
    | `Xml (v1, v2)        -> pp_binop ppf "`Xml" v1 v2
    | `Arrow (v1, v2)      -> pp_binop ppf "`Arrow" v1 v2
    | `Diff (v1, v2)       -> pp_binop ppf "`Diff" v1 v2
    | `Record (fields, op) ->
        fprintf ppf "@[`Record(%s,[" (if op then "open" else "close");
        fprintf ppf "%a"
          (pp_print_list ~pp_sep (fun ppf (lab, opt, t) ->
               fprintf ppf "%a=%s%a" Ident.Label.print_attr lab
                 (if opt then "?" else "")
                 loop t))
          fields;
        fprintf ppf ")@]"
  and pp_binop ppf o v1 v2 = fprintf ppf "@[%s(%a,@ %a)@]" o loop v1 loop v2
  and pp_lst ppf l =
    fprintf ppf "@[[";
    fprintf ppf "%a" (pp_print_list ~pp_sep loop) l;
    fprintf ppf "]@]"
  in
  loop ppf v

let forward () = { id = Oo.id (object end); def = `Cup []; node = None }

let def v d = v.def <- d

let cons d =
  let v = forward () in
  def v d;
  v

let ty d = cons (`Type d)

let cup vl = cons (`Cup vl)

let times d1 d2 = cons (`Times (d1, d2))

let xml d1 d2 = cons (`Xml (d1, d2))

let arrow d1 d2 = cons (`Arrow (d1, d2))

let record fields op = cons (`Record (fields, op))

let cap vl = cons (`Cap vl)

let diff d1 d2 = cons (`Diff (d1, d2))

let define v1 v2 = def v1 (`Cup [ v2 ])

let solve v = Types.internalize (make_node v)

let diff_t d1 d2 =
  match d2.def with
  | `Cup [] -> d1
  | `Cap [] -> ty Types.empty
  | _       -> diff d1 d2

let cup_l = function [ v ] -> v | l -> cup l

let do_line acc any_k cons (lpos, lneg) =
  let pos = cap (any_k::(List.map cons lpos)) in
  let neg = cup_l (List.map cons lneg) in
  diff_t pos neg :: acc

let do_line_cup any_k cons at = cup_l @@ do_line [] any_k cons at

let decompose ?(stop = fun _ -> None) t =
  let memo = MemHash.create 17 in
  let app_stop t f =
    match stop t with
    Some v -> v
    | None -> f t
  in
  let do_vars acc do_atom dnf =
    List.fold_left
      (fun acc (vars, at) ->
        do_line acc (do_atom at) (fun v -> app_stop (var v) ty) vars)
      acc dnf
  in
  let rec loop t =
    match stop t with
    | Some s -> s
    | None   -> (
        try MemHash.find memo t
        with Not_found ->
          let node_t = forward () in
          let () = MemHash.add memo t node_t in
          let rhs = loop_struct t in
          node_t.def <- rhs.def;
          node_t)
  and loop_struct t =
    cup_l
    @@ Iter.fold
         (fun acc pack t ->
           match pack with
           | Iter.Int m | Char m | Atom m | Abstract m ->
               let module K = (val m) in
               do_vars acc
                 (fun at -> app_stop K.(mk (Dnf.mono at)) ty)
                 K.(Dnf.get_partial (get_vars t))
           | Times m | Xml m | Function m ->
               let cons =
                 match pack with Times _ -> times | Xml _ -> xml | _ -> arrow
               in
               let module K = (val m) in
               do_vars acc
                 (do_line_cup (ty K.any) (fun (d1, d2) ->
                      let tt = K.(mk (Dnf.atom (d1, d2))) in
                      app_stop tt (fun _ ->
                      cons (app_stop (descr d1) loop) (app_stop (descr d2) loop))))
                 K.(Dnf.get_full (get_vars t))
           | Record _ ->
               do_vars acc
                 (do_line_cup (ty Rec.any) (fun (op, fields) ->
                  let tt = Rec.(mk (Dnf.atom (op, fields))) in
                  app_stop tt (fun _ ->
                      record
                        (Ident.LabelMap.mapi_to_list
                           (fun l t ->
                             let t = Types.descr t in
                             (l, Types.Record.has_absent t, app_stop t loop))
                           fields)
                        op)))
                 Rec.(Dnf.get_full (get_vars t))
           | Absent ->
               if Record.has_absent t then ty (Record.or_absent empty) :: acc
               else acc)
         [] t
  in

  loop t
