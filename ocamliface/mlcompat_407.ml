(* Compatibilty for OCaml 4.07 *)
open Cduce_core

let longident_parse = Ocaml_common.Longident.parse

module Mlstub = struct
  let noloc id = id

  let str_open l =
    let open Ocaml_common.Ast_helper in
    Str.open_ (Opn.mk l)

  let pat_construct lid pat =
    let open Ocaml_common.Ast_helper in
    Pat.construct lid pat
end

module Mltypes = struct
  open Ocaml_common

  let get_path_from_mty_alias = function
    | Types.Mty_alias (_, p) -> p
    | _                      -> assert false

  let lookup_value li env = Env.lookup_value li env

  let lookup_module li env = Env.lookup_module ~load:true li env

  let load_path () =
    let add_dir s =
      if not (List.mem s !Config.load_path) then
        Config.load_path := s :: !Config.load_path
      in
      List.iter add_dir (List.rev !Cduce_loc.obj_path);
      add_dir Config.standard_library

  let find_in_path file = Misc.find_in_path_uncap !Config.load_path file

  let get_path_from_pdot e =
    match e with Path.Pdot (p, _, _) -> p | _ -> assert false

  let is_sig_value_val_reg e =
    match e with
    | Types.Sig_value (_, { val_type = _; val_kind = Val_reg }) -> true
    | _ -> false

  let get_id_t_from_sig_value e =
    match e with
    | Types.Sig_value (id, { val_type = t }) -> (id, t)
    | _ -> assert false

  let get_sig_type e =
    match e with Types.Sig_type (id, t, rs) -> (id, t, rs) | _ -> assert false

  let is_sig_value_deprecated e =
    match e with
    | Types.Sig_value (_, { val_attributes; _ }) ->
        List.exists
          (fun att ->
            let txt = Asttypes.((fst att).txt) in
            txt = "ocaml.deprecated" || txt = "deprecated")
          val_attributes
    | _ -> assert false

  let get_type_variant_cstr t =
    match t with Types.Type_variant cstr -> cstr | _ -> assert false
end
