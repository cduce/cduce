(* Compatibilty for OCaml 4.13 *)
open Cduce_core

let longident_parse s =
  let open Ocaml_common in
  let rec loop first s =
    match String.rindex_opt s '.' with
    | None   -> Longident.Lident s
    | Some i -> (
        try Parse.longident (Lexing.from_string s)
        with _ -> (
          try Parse.val_ident (Lexing.from_string s)
          with _ -> (
            try Parse.constr_ident (Lexing.from_string s)
            with e ->
              (* last resort *)
              if first then
                let p, s =
                  ( String.sub s 0 i,
                    String.sub s (i + 1) (String.length s - i - 1) )
                in
                let s = p ^ ".(" ^ s ^ ")" in
                loop false s
              else raise e)))
  in
  loop true s

module Mlstub = struct
  let noloc id = Some id

  let str_open l =
    let open Ocaml_common.Ast_helper in
    Str.open_ (Opn.mk (Mod.ident l))

  let pat_construct lid pat =
    let open Ocaml_common.Ast_helper in
    Pat.construct lid (match pat with None -> None | Some p -> Some ([], p))
end

module Mltypes = struct
  open Ocaml_common

  let get_path_from_mty_alias = function
    | Types.Mty_alias p -> p
    | _                 -> assert false

  let lookup_value li env = Env.find_value_by_name li env

  let lookup_module li env =
    let loc =
      Warnings.
        {
          loc_start = Lexing.dummy_pos;
          loc_end = Lexing.dummy_pos;
          loc_ghost = true;
        }
    in
    Env.lookup_module_path ~use:true ~load:true ~loc li env

  let load_path () =
    let add_dir s =
      if not (List.mem s (Load_path.get_paths ())) then
        Load_path.add_dir s
      in
      List.iter add_dir (List.rev !Cduce_loc.obj_path);
      add_dir Config.standard_library
  let find_in_path file = Misc.find_in_path_uncap (Load_path.get_paths ()) file

  let get_path_from_pdot e =
    match e with Path.Pdot (p, _) -> p | _ -> assert false

  let is_sig_value_val_reg e =
    match e with
    | Types.Sig_value (_, { val_type = _; val_kind = Val_reg; _ }, _) -> true
    | _ -> false

  let get_id_t_from_sig_value e =
    match e with
    | Types.Sig_value (id, { val_type = t; _ }, _) -> (id, t)
    | _ -> assert false

  let get_sig_type e =
    match e with
    | Types.Sig_type (id, t, rs, _) -> (id, t, rs)
    | _ -> assert false

  let is_sig_value_deprecated e =
    match e with
    | Types.Sig_value (_, { val_attributes; _ }, _) ->
        List.exists
          (fun att ->
            let txt = Parsetree.(att.attr_name.txt) in
            txt = "ocaml.deprecated" || txt = "deprecated")
          val_attributes
    | _ -> assert false

  let get_type_variant_cstr t =
    match t with Types.Type_variant (cstr, _) -> cstr | _ -> assert false
end
