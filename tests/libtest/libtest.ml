(* Inspired by F. Pottier's test infrastructure for Menhir *)
type kind = Good | Bad

let kind = ref Good

let files = ref []

let spec =
  Arg.
    [
      ( "--kind",
        String
          (function
          | "good" -> kind := Good
          | "bad"  -> kind := Bad
          | _      -> raise @@ Bad "the kind must be `good` or `bad`."),
        "  <good|bad> sets the kind of tests to generate (defeault: good)" );
    ]

let input_files s = files := s :: !files

let usage = Format.sprintf "%s [options] <file> [...]" Sys.argv.(0)

let parse_argv ?(extra = []) () =
  let spec = Arg.align (spec @ extra) in
  Arg.parse spec input_files usage

let check_file f ext = Filename.check_suffix f ext && Sys.file_exists f

let protect f g =
  try
    let r = f () in
    g ();
    r
  with e ->
    g ();
    raise e

let cduce_prog ext = if ext then "cduce_external" else "cduce"

let cduce_compile_rule ?(ext = false) base =
  let cduce = cduce_prog ext in
  let cflags = base ^ ".cflags" in
  let cdo = base ^ ".cdo" in
  let cd = base ^ ".cd" in
  let cf = if Sys.file_exists cflags then "%%{read:" ^ cflags ^ "}" else "" in
  Format.printf
    "(rule (deps %s) (target %s)\n\
    \   (action (ignore-outputs (with-accepted-exit-codes 0 (run %s --compile \
     %s %%{deps})))))\n"
    cd cdo cduce cf

let cduce_run_rule ?(ext = false) base kind =
  let cduce = cduce_prog ext in
  let rflags = base ^ ".rflags" in
  let cdo = base ^ ".cdo" in
  let out = base ^ ".out" in
  let rf =
    if Sys.file_exists rflags then "%{read-lines:" ^ rflags ^ "}" else ""
  in
  let write_output, close, code =
    match kind with
    | Good -> ("ignore-stderr (with-stdout-to", ")", "0")
    | Bad  -> ("with-outputs-to", "", "(not 0)")
  in
  Format.printf
    "(rule (deps %s) (target %s)\n\
    \   (action (%s %%{target} (with-accepted-exit-codes %s (run %s --run %s \
     %%{deps})))%s))\n"
    cdo out write_output code cduce rf close

let diff_rule base =
  let exp = base ^ ".exp" in
  let out = base ^ ".out" in
  Format.printf "(rule (alias %s) (action (diff %s %s)))\n" base exp out

let gen_cduce_test ?(ext = false) kind acc f =
  if check_file f ".cd" then begin
    let base = Filename.remove_extension f in
    Format.printf "; begin: %s\n" f;
    cduce_compile_rule ~ext base;
    cduce_run_rule base ~ext kind;
    diff_rule base;
    Format.printf "; end: %s\n\n" f;

    Format.sprintf "(alias %s)" base :: acc
  end
  else acc

let cduce_type_rule base file =
  let out = base ^ ".out" in
  Format.printf
    "(rule (alias %s) (deps %s) (target %s)\n\
     (action (with-outputs-to %%{target} (with-accepted-exit-codes 0 (run \
     ../src/test_type.exe %%{deps})))))"
    base file out

let gen_type_test acc f =
  if check_file f ".cd" then begin
    let base = Filename.remove_extension f in
    Format.printf "; begin: %s\n" f;
    cduce_type_rule base f;
    diff_rule base;
    Format.printf "; end: %s\n\n" f;
    Format.sprintf "(alias %s)" base :: acc
  end
  else acc

let gen_cduce_tests kind files =
  let files = List.sort_uniq String.compare files in
  let aliases = List.fold_left (gen_cduce_test kind) [] files in
  Format.printf
    "(alias (name runtest)\n   (deps\n    (source_tree ../../common)\n";
  List.iter (Format.printf "    %s\n") (List.rev aliases);
  Format.printf "))\n"

let gen_type_tests files =
  let files = List.sort_uniq String.compare files in
  let aliases = List.fold_left gen_type_test [] files in
  Format.printf
    "(alias (name runtest)\n   (deps\n    (source_tree ../../common)\n";
  List.iter (Format.printf "    %s\n") (List.rev aliases);
  Format.printf "))\n"
