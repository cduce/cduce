open Cduce_core

let init_top ppf =
  let () = Cduce_config.init_all () in
  Format.fprintf ppf "        CDuce version %s\n@." Version.cduce_version

let eval_top ppf ppf_err input =
  let () = Cduce_loc.push_source (`String input) in
  let _ =
    try Cduce_driver.topinput ppf ppf_err (Stream.of_string input)
    with Cduce_driver.Escape _ -> false
  in
  Cduce_loc.pop_source ()
