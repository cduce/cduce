open Cduce_core

let () = Stats.gettimeofday := Unix.gettimeofday

let external_init = ref None

let out_dir = ref [] (* directory of the output file *)

let src = ref []

let args = ref []

let compile = ref false

let run = ref false

let script = ref false

let mlstub = ref false

let topstub = ref false

let binarystub = ref false

let version () =
  Printf.eprintf "CDuce, version %s\n" Version.cduce_version;
  Printf.eprintf "Using OCaml %s compiler\n" Version.ocaml_compiler;
  Printf.eprintf "Supported features: \n";
  List.iter
    (fun (n, d) -> Printf.eprintf "- %s: %s\n" n d)
    (Cduce_config.descrs ());
  exit 0

let parse_argv () =
  let usage_msg =
    "Usage:\ncduce [OPTIONS ...] [FILE ...] [--arg argument ...]\n\nOptions:"
  in
  let help = ref (fun () -> assert false) in
  let specs =
    Arg.align
      ([
         ("--compile", Arg.Set compile, "  compile the given CDuce file");
         ("-c", Arg.Set compile, "  same as --compile");
         ("--run", Arg.Set run, "  execute the given .cdo files");
         ( "--verbose",
           Arg.Set Cduce_driver.verbose,
           "  show types of exported values (for --compile)" );
         ( "--obj-dir",
           Arg.String (fun s -> out_dir := s :: !out_dir),
           "  directory for the compiled .cdo file (for --compile)" );
         ( "-I",
           Arg.String (fun s -> Cduce_loc.obj_path := s :: !Cduce_loc.obj_path),
           "  add one directory to the lookup path for .cdo/.cmi and include \
            files" );
         ( "--stdin",
           Arg.Unit (fun () -> src := "" :: !src),
           "  read CDuce script on standard input" );
         ( "--arg",
           Arg.Rest (fun s -> args := s :: !args),
           "  following arguments are passed to the CDuce program" );
         ( "--script",
           Arg.Rest
             (fun s ->
               if not !script
               then (
                 script := true;
                 src := s :: !src)
               else args := s :: !args),
           "  the first argument after is the source, then the arguments" );
         ( "--no",
           Arg.String Cduce_config.inhibit,
           "  disable a feature (cduce -v to get a list of features)" );
         ( "--debug",
           Arg.Unit (fun () -> Stats.set_verbosity Stats.Summary),
           "  print profiling/debugging information" );
         ( "-v",
           Arg.Unit version,
           "  print CDuce version, and list built-in optional features" );
         ( "--version",
           Arg.Unit version,
           "  print CDuce version, and list built-in optional features" );
         ( "--mlstub",
           Arg.Set mlstub,
           " produce stub ML code from a compiled unit" );
         ( "--topstub",
           Arg.Set topstub,
           "  produce stub ML code for a toplevel from a primitive file" );
         ( "--binarystub",
           Arg.Set binarystub,
           "  output stub ML code in binary format (default text format)" );
         ( "-help",
           Arg.Unit (fun () -> raise (Arg.Bad "unknown option '-help'")),
           "" );
       ]
      @ !Cduce_driver.extra_specs
      @ [
          ("-h", Arg.Unit (fun () -> !help ()), "  display this list of options");
          ( "--help",
            Arg.Unit (fun () -> !help ()),
            "  display this list of options" );
        ])
  in
  (help :=
     fun () ->
       Arg.usage specs usage_msg;
       exit 0);
  Arg.parse specs (fun s -> src := s :: !src) usage_msg

let ppf = Format.std_formatter

let ppf_err = Format.err_formatter

let err s =
  prerr_endline s;
  exit 1

let mode () =
  parse_argv ();
  if !mlstub
  then
    match !src with
    | [ x ] -> `Mlstub x
    | _     -> err "Please specify one .cdo file"
  else if !topstub
  then
    match !src with
    | [ x ] -> `Topstub x
    | _     -> err "Please specify one primitive file"
  else
    match (!compile, !out_dir, !run, !src, !args) with
    | false, _ :: _, _, _, _ ->
        err "--obj-dir option can be used only with --compile"
    | false, [], false, [], args -> `Toplevel args
    | false, [], false, [ x ], args -> `Script (x, args)
    | false, [], false, _, _ ->
        err "Only one CDuce program can be executed at a time"
    | true, [ o ], false, [ x ], [] -> `Compile (x, Some o)
    | true, [], false, [ x ], [] -> `Compile (x, None)
    | true, [], false, [], [] ->
        err "Please specify the CDuce program to be compiled"
    | true, [], false, _, [] ->
        err "Only one CDuce program can be compiled at a time"
    | true, _, false, _, [] -> err "Please specify only one output directory"
    | true, _, false, _, _ ->
        err "No argument can be passed to programs at compile time"
    | false, _, true, [ x ], args -> `Run (x, args)
    | false, _, true, [], _ ->
        err "Please specifiy the CDuce program to be executed"
    | false, _, true, _, _ ->
        err "Only one CDuce program can be executed at a time"
    | true, _, true, _, _ ->
        err "The options --compile and --run are incompatible"

let bol = ref true

let outflush s =
  output_string stdout s;
  flush stdout

let has_newline b =
  let rec loop i found =
    if i >= 1
    then
      let c = Buffer.nth b i in
      if c == ';' && Buffer.nth b (i - 1) == ';'
      then found
      else loop (i - 1) (c == '\n')
    else false
  in
  loop (Buffer.length b - 1) false

let toploop () =
  let restore =
    if Sys.win32
    then fun () -> ()
    else
      try
        let tcio = Unix.tcgetattr Unix.stdin in
        Unix.tcsetattr Unix.stdin Unix.TCSADRAIN
          { tcio with Unix.c_vquit = '\004' };
        fun () -> Unix.tcsetattr Unix.stdin Unix.TCSADRAIN tcio
      with Unix.Unix_error (_, _, _) -> fun () -> ()
  in
  let quit () =
    outflush "\n";

    restore ();
    exit 0
  in
  Format.fprintf ppf "        CDuce version %s\n@." Version.cduce_version;
  if not Sys.win32
  then Sys.set_signal Sys.sigquit (Sys.Signal_handle (fun _ -> quit ()));
  Sys.catch_break true;
  Cduce_driver.toplevel := true;
  Librarian.run_loaded := true;
  let buf_in = Buffer.create 1024 in
  Cduce_loc.push_source (`Buffer buf_in);
  let read _i =
    if !bol then if !Sedlexer.in_comment then outflush "* " else outflush "> ";
    try
      let c = input_char stdin in
      Buffer.add_char buf_in c;
      bol := c = '\n';
      Some c
    with Sys.Break -> quit ()
  in
  let input = Stream.from read in
  let rec loop () =
    outflush "# ";
    bol := false;
    Buffer.clear buf_in;
    ignore (Cduce_driver.topinput ppf ppf_err input);
    if not (has_newline buf_in) then
      (* ";;\n" was eaten by a regular expression in the lexer *)
    while (input_char stdin != '\n') do () done;
    loop ()
  in
  (try loop () with End_of_file -> ());
  restore ()

let main () =
  at_exit (fun () -> Stats.dump Format.std_formatter);
  Cduce_loc.set_viewport (Html.create false);
  let m = mode () in
  (* May call Cduce_config.inhibit while parsing the command line *)
  let () =
    match !external_init with
    | Some f
      when List.exists (fun (n, _) -> n = "ocaml") (Cduce_config.descrs ()) ->
        f () (* calls Cduce_config.init_all ()*)
    | _ -> Cduce_config.init_all ()
  in

  match m with
  | `Toplevel args    ->
      Cduce_driver.set_argv args;
      toploop ()
  | `Script (f, args) ->
      Cduce_driver.set_argv args;
      Cduce_driver.compile_run f
  | `Compile (f, o)   -> Cduce_driver.compile f o
  | `Run (f, args)    ->
      Cduce_driver.set_argv args;
      Cduce_driver.run f
  | `Mlstub f         -> Librarian.prepare_stub !binarystub f
  | `Topstub f        -> !Librarian.make_wrapper !binarystub f
