val top_phrases : char Stream.t -> Ast.pmodule_item list

val prog : char Stream.t -> Ast.pmodule_item list

val pat : char Stream.t -> Ast.ppat

val expr : char Stream.t -> Ast.pexpr

val sync : unit -> unit

val dump_tokens : Format.formatter -> char Stream.t -> unit