let run_cmd c =
  let stdin = Unix.open_process_in c in 
  let rec loop acc =
    match input_line stdin with
      s -> loop (s::acc)
      | exception End_of_file -> List.rev acc
  in
  loop []



let link_flags : [`I of string | `P of string | `L of string ] list ref = ref []

let set_i s = link_flags := (`I s) :: !link_flags
let set_p s = link_flags := (`P s) :: !link_flags
let set_l s = link_flags := (`L s) :: !link_flags

let native = ref true

let target = ref ""

let prim_file = ref ""

let ocamlfind_prog = ref "ocamlfind"
let cduce_prog = ref "cduce"


let set_args s =
  if !target = "" then
    target := s
  else if !prim_file = "" then
    prim_file := s
  else
    raise (Arg.Bad "too many arguments")

let specs = Arg.(align [
    ("-byte", Unit (fun () -> native := false), "  Compile with ocamlc (default to ocamlopt)");
    ("-l", String set_l, "<file.{cmo,cma,cmx,cmxa}>  Link with the specified object");
    ("-p", String set_p, "<package>  Link with the specified ocamlfind package");
    ("-I", String set_i, "<dir>  Add <dir> to the list of include directories");
    ("-cduce", Set_string cduce_prog, "<prog>  Use <prog> as the cduce compiler");
    ("-ocamlfind", Set_string ocamlfind_prog, "<prog>  Use <prog> as the ocamlfind command");
])

let usage = Printf.sprintf "%s [OPTION...] <target> <prims>\n\n\
   Create a CDuce toplevel <target> with the OCaml primitives listed in <prims> included.\n\
   \nOptions:" Sys.argv.(0)

exception Error of string

let compatible_ext s =
  if !native then
    Filename.check_suffix s ".cmx" || Filename.check_suffix s ".cmxa"
  else
   Filename.check_suffix s ".cmo" || Filename.check_suffix s ".cma"
 
let main () = 
  let () = Arg.parse specs set_args usage in
  if !target = ""  then
    raise (Error ("missing target toplevel name"));
  if !prim_file = "" then
    raise (Error ("missing primitive file name"));
  let compiler, predicate, pstr =
    if !native then "ocamlopt", "native", "native" else
    "ocamlc", "byte", "bytecode" 
  in
  let ocamlfind =
    Printf.sprintf "%s query -predicates %s -format " !ocamlfind_prog predicate
  in
  let include_flags = 
    List.flatten @@
    List.map (function
      `I s -> [ "-I"; s ]
    | `P s ->  List.flatten @@ List.map (fun s -> [ "-I"; s]) (run_cmd @@ ocamlfind ^ "'%d' " ^ s) 
    | _ -> []
  ) !link_flags
   in
  let packages_flags = 
       List.flatten @@
    List.map (function
    | `P s -> [ s ]
    | _ -> []
  ) ((`P "cduce.lib") :: (`P "cduce.lib.ocamliface") :: (`P "ocaml-compiler-libs.common") :: !link_flags)
  in
  let objects = List.flatten @@
     List.map (function
    | `L s -> if compatible_ext s then [ s ] else raise (Error (Printf.sprintf "file '%s' is incompatible in %s mode" s pstr))
    | _ -> []
  ) !link_flags
  in
  let str_i_flags = String.concat " " include_flags in
  let str_p_flags = "-package " ^ (String.concat "," packages_flags) in
  let str_objects = String.concat " " objects in
  let cmd =
    Printf.sprintf "%s %s -o %s -linkpkg %s %s %s -pp '%s %s --topstub' -impl %s"
    !ocamlfind_prog
    compiler !target
    str_i_flags
    str_p_flags
    str_objects
    !cduce_prog
    str_i_flags
    !prim_file
   in exit (Sys.command cmd)


let () =
  try
    main ()
  with Error msg ->
    Printf.eprintf "Error: %s\nusage: " msg;
    Arg.usage specs usage;
    exit 2
